# /packages/intranet-trans-trados/tcl/intranet-trans-trados-file-procs.tcl


ad_library {
    Bring together all file operations
    related to the Translation sector for TRADOS

	@author malte.sussdorff@cognovis.de
}

ad_proc -public im_sencha_tables_component {
	{-company_id ""}
	{-project_id ""}
	{-user_id ""}
	{-return_url ""}
} {
	Display the TMs of a company or project
} {	
	if {$company_id ne ""} {
		set params [list  [list base_url "/intranet-translation/"]  [list company_id $company_id] [list return_url [im_biz_object_url $company_id]]]
	} elseif {$project_id ne ""} {
		set params [list  [list base_url "/intranet-translation/"]  [list project_id $project_id] [list return_url [im_biz_object_url $project_id]]]
	}
	
	if {[info exists params]} {
		set result [ad_parse_template -params $params "/packages/intranet-trans-trados/lib/trados-folder"]	
	} else {
		set result ""
	}
}


ad_proc -public im_sencha_tables_rest_call {
	-valid_vars
	-sql
	-order_by
	{ -query_hash_pairs {} }
	{ -rest_user_id "0"}

} {
	Handler for GET custom rest calls
	
	@param valid_vars Variables which are valid
	@param sql SQL command to call. Must be ready to accept the \$where_clause and contain object_id and object_name as variables
	@param order_by Field in the SQL command to use for ordering
} {
	array set query_hash $query_hash_pairs
	set base_url "[im_rest_system_url]/intranet-rest"

	if {$rest_user_id eq "0"} {set rest_user_id [ad_conn user_id]}
	
	# Get locate for translation
	set locale [lang::user::locale -user_id $rest_user_id]

	# -------------------------------------------------------
	# Check if there is a where clause specified in the URL and validate the clause.
	set where_clause ""
	if {[info exists query_hash(query)]} { set where_clause $query_hash(query)}


	# -------------------------------------------------------
	# Check if there are "valid_vars" specified in the HTTP header
	# and add these vars to the SQL clause
	set where_clause_list [list]
	foreach v $valid_vars {
		if {[info exists query_hash($v)]} { lappend where_clause_list "$v=$query_hash($v)" }
	}
	if {"" != $where_clause && [llength $where_clause_list] > 0} { append where_clause " and " }
	append where_clause [join $where_clause_list " and "]


	# Check that the query is a valid SQL where clause
	set valid_sql_where [im_rest_valid_sql -string $where_clause -variables $valid_vars]
	if {!$valid_sql_where} {
		im_rest_error -format $format -http_status 403 -message "The specified query is not a valid SQL where clause: '$where_clause'"
		return
	}
	if {"" != $where_clause} { set where_clause "and $where_clause" }

	# Select SQL: Pull out categories.
	append sql " $where_clause order by $order_by"

	# Append pagination "LIMIT $limit OFFSET $start" to the sql.
	set unlimited_sql $sql
	append sql [im_rest_object_type_pagination_sql -query_hash_pairs $query_hash_pairs]

	set value ""
	set result ""
	set obj_ctr 0
	
	db_foreach objects $sql {

		set komma ",\n"
		if {0 == $obj_ctr} { set komma "" }
		set dereferenced_result ""
		foreach v $valid_vars {
			eval "set a $$v"
			regsub -all {\n} $a {\n} a
			regsub -all {\r} $a {} a
			append dereferenced_result ", \"$v\": \"[im_quotejson $a]\""
		}
		append result "$komma{\"id\": \"$object_id\", \"object_name\": \"[im_quotejson $object_name]\"$dereferenced_result}" 
		incr obj_ctr
	}

	set result "{\"success\": true,\n\"total\": $obj_ctr,\n\"message\": \"im_sencha_tables_rest_call: Data loaded\",\n\"data\": \[\n$result\n\]\n}"
	im_rest_doc_return 200 "application/json" $result
	return
}

ad_proc -public im_sencha_company_projects_component {
	{-project_status_id ""}
	{-project_type_id ""}
	{-project_lead_id ""}
	{-company_id ""}
	{-return_url ""}
	{-global_hide_columns ""}
} {
	Returns the component for active projects
} {
	set params [list [list project_lead_id $project_lead_id] [list project_status_id $project_status_id] [list project_type_id $project_type_id] [list company_id $company_id] [list global_hide_columns $global_hide_columns] [list return_url $return_url]]
	set result [ad_parse_template -params $params "/packages/intranet-sencha-tables/lib/company-projects"]
	return [string trim $result]
}


ad_proc -public im_sencha_personal_projects_component {
	{-project_status_id ""}
	{-project_type_id ""}
	{-project_lead_id ""}
	{-company_id ""}
	{-member_id ""}
	{-global_hide_columns ""}
} {
	Returns the component for active projects
} {
	set params [list [list project_lead_id $project_lead_id] [list project_status_id $project_status_id] [list project_type_id $project_type_id] [list company_id $company_id] [list freelancer_id $member_id] [list global_hide_columns $global_hide_columns]]
	set result [ad_parse_template -params $params "/packages/intranet-sencha-tables/lib/projects"]
	return [string trim $result]
}

ad_proc -public im_sencha_rfq_component {
} {
    Returns the component for the RFQ
} {
    set result [ad_parse_template "/packages/intranet-sencha-tables/lib/rfq"]
    return [string trim $result]
}



ad_proc -public im_company_generate_path {
	-company_name 
	{ -company_city ""}
} {
	Generates unique company path
	@param company_name name of a company for which we want to genere path
	@param company_city city (main headquarters) of a company for which we want to genere path
} {

    # Trim, lowercase and remove unwanted chars
    set potential_company_folder_name [generate_folder_name -name $company_name]

    # We need base path for companies
    set companies_folder [parameter::get_from_package_key -package_key "intranet-filestorage" -parameter "CompanyBasePathUnix" -default ""]
    
	if {[file exist "$companies_folder/$potential_company_folder_name"]} {
		# First we check if 'city' variable exist somwhere
		if {$company_city eq ""} {
        	upvar city upvar_company_city
        	if {[info exists upvar_company_city]} {
               set company_city "$upvar_company_city"
        	}
		}
		if {$company_city ne ""} {
    		set prepared_city_name [generate_folder_name -name $company_city]
            set potential_path_elements [list $potential_company_folder_name $prepared_city_name]
            set potential_company_folder_name [join $potential_path_elements "_"]		
		} 
	
    	# Now we check again if such name already exist
    	if {[file exist "$companies_folder/$potential_company_folder_name"]} {
    		# Find folders that match company name
    		set tcl_files [glob -type d -directory $companies_folder $potential_company_folder_name*]
    		set next_one [expr [llength $tcl_files] + 1]
    		set potential_company_folder_name [join [list $potential_company_folder_name $next_one] "_"]
    	} 

	}
	set company_path $potential_company_folder_name
	return $company_path

}


ad_proc -public im_user_generate_screen_name {
	-first_names
	-last_name
	-company_name
} {
	Generates unique screen name for the user
	@param first_names first names of user whom we need to generate screen name
	@param last_name of user for whom we need to generate screen name
	@param company_name company name of user for whom we need to generate screen name
} {

	set screen_name "$first_names $last_name"
	set screen_name_exists_p [db_string check_screen_name "select 1 from cc_users where screen_name =:screen_name" -default 0]
	if {$screen_name_exists_p} {
		set screen_name_second "$first_names $last_name ($company_name)"
		set screen_name_exists_p [db_string check_screen_name "select 1 from cc_users where screen_name =:screen_name_second" -default 0]
		if {$screen_name_exists_p} {
			set count_same_screen_names [db_string count_screen_names "select count(*) from cc_users where screen_name =:screen_name" -default 0]
			incr count_same_screen_names
			set screen_name_third "$first_names $last_name $count_same_screen_names"
			set screen_name $screen_name_third
		} else {
			set screen_name $screen_name_second
		}
	}
	return $screen_name
}


ad_proc -public im_company_generate_office_name {
	-company_name
	{ -company_city ""}
} {
	Generates unique screen name for the user
	@param company_name name of company for which we want to generate unique office name
	@param company_city city of the company for which we want to generate unique office name
} {

	set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"
	set office_name_exists_p [db_string check_office_name "select 1 from im_offices where office_name =:office_name" -default 0]
	if {$office_name_exists_p} {
		if {$company_name ne ""} {
			set office_name_second "$office_name $company_city"
		} else {
			set count_offices_names [db_string count_offices "select count(*) from im_offices where office_name = :office_name" -default 0]
			incr count_offices_names
			set office_name_second "$office_name $count_offices_names"
		}
		set office_name_exists_p [db_string check_office_name "select 1 from im_offices where office_name =:office_name_second" -default 0]
		if {$office_name_exists_p} {
			set count_offices_names [db_string count_offices "select count(*) from im_offices where office_name = :office_name" -default 0]
			incr count_offices_names
			set office_name_third "$office_name $count_offices_names"
			set office_name $office_name_third
		} else {
			set office_name $office_name_second
		}
	}
	return $office_name
}





