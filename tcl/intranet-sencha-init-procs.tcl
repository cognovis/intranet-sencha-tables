ad_library {
    
    initialize the filters for files
    
    @author  malte.sussdorff@cognovis.de
    @creation-date 2017-08-01
    @cvs-id $Id$
}

ns_register_proc POST /sencha-tables/upload im_sencha_upload_file
ns_register_proc GET /sencha-tables/status im_sencha_upload_status
ns_register_filter preauth POST /sencha-tables/upload/* im_sencha_upload_update


