ad_library {
    
    Procedures to handle file upload
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-08-01
    @cvs-id $Id$
}


ad_proc im_sencha_upload_file {
} {
    Procedure to upload the file
} {
    set key [ns_conn urlv 1]
    nsv_unset status $key
    set tmp_filename [ns_queryget ${key}.tmpfile]
    file copy $tmp_filename "${tmp_filename}.copy"
    set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"File: Data loaded\",\n\"data\": \[\n{\"file\":\"$filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n}"
im_rest_doc_return 200 "application/json" $result
}


ad_proc im_sencha_upload_update {why} {
    Filter procedure to update the status of the file
    upload. 
    
    Will set the global nsv_set of status for they file ($key) to a list of expected and received number of bytes
} {
    set key [ns_conn urlv 1]
    set expected [ns_conn contentlength]
    set received [ns_conn contentavail]
    set status [list $expected $received]
    nsv_set status $key $status
    return filter_ok
}

ad_proc im_sencha_upload_status {} {
    Returns the JSON with the current status of the upload
    for the file identified by key
} {
    set key [ns_conn urlv 1]
    if {[catch {set status [nsv_get status $key]}]} {
        set status "unknown"
    }
    set result "{\"success\": true,\n\"total\": 1,\n
  \"message\": \"File progress\",\n
  \"data\": \[\n{\"expected\":\"[lindex $status 0]\",
        \"received\":\"[lindex $status 1]\"}\n\]\n}"
    im_rest_doc_return 200 "application/json" $result
}
