ad_library {
    Rest POST Procedures for the sencha-tables package
    
    @author malte.sussdorff@cognovis.de
}




# ---------------------------------------------------------------
# ERROR HANDLING!!!!
# This is missing throughout, so we need to properly catch
# And return an error message to the user....
# ---------------------------------------------------------------

namespace eval sencha_errors {

    variable objects_warnings ""
    variable objects_errors ""
    variable existing_ids [list]


    ad_proc add_error {
        -object_id
        {-field ""}
        -problem
    } {
       adding an error, allowing commulation of errors for later provision with display_errors_and_warnings
       @param object_id Object which has the problem. if there is no object_id yet, use a random generated token
    } {
        variable objects_errors
        variable existing_ids

        lappend existing_ids $object_id

	upvar proc_name proc_name
	uplevel {set proc_name [info level 0]}

	ns_log Error "$proc_name :: $object_id :: $problem"
        dict lappend objects_errors $object_id $field $problem
    }

    ad_proc add_warning {
        -object_id
        {-field ""}
        -problem
    } {
       adding a warning, allowing commulation of warnings for later provision with display_errors_and_warnings
       @param object_id Object which has the problem. if there is no object_id yet, use a random generated token
    } {
        variable objects_warnings
        variable existing_ids

        lappend existing_ids $object_id

	upvar proc_name proc_name
	uplevel {set proc_name [info level 0]}

	ns_log Warning "$proc_name :: $object_id :: $problem"

        dict lappend objects_warnings $object_id $field $problem
    }

    ad_proc clear {
        -object_id
    }  {
       clearing commulated errors and warnings for given @param object_id
    } {
        variable objects_warnings
        variable objects_errors
        set objects_warnings ""
        set objects_errors ""
    }

    ad_proc display_errors_and_warnings {
        -object_id
        {-action_type ""}
        {-modal_title ""}
    } {
        displaying errors and warnings for given @param object_id in case any problems appeared
    } {

        variable objects_warnings
        variable objects_errors
        variable existing_ids
        
        set obj_ctr_errors 0
        set obj_ctr_warnings 0
        set json_errors ""
        set json_warnings ""
        
        # building json structure for warnings
        set size_of_warnings [dict size $objects_warnings]
        if {$size_of_warnings > 0} {
            set single_object_warnings [dict get $objects_warnings $object_id]
            dict for {field warning} $single_object_warnings {
                set komma ",\n"
                if {0 == $obj_ctr_warnings} { set komma "" }
                if {$field ne ""} {
                    append json_warnings "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $warning]\"}"
                } else {
                    append json_warnings "$komma{\"message\": \"[im_quotejson $warning]\"}"
                }
                incr obj_ctr_warnings
            }
        }
       # building json structure for errors
       set size_of_errors [dict size $objects_errors]
       if {$size_of_errors > 0} {
	   set dict_keys [dict keys $objects_errors]
	   if {[lsearch $dict_keys $object_id]>-1} {
	       set single_object_errors [dict get $objects_errors $object_id]
	       dict for {field error} $single_object_errors {
		   set komma ",\n"
		   if {0 == $obj_ctr_errors} { set komma "" }
		   if {$field ne ""} {
		       append json_errors "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $error]\"}"
		   } else {
		       append json_errors "$komma{\"message\": \"[im_quotejson $error]\"}"
		   }
		   incr obj_ctr_errors
	       }
	   }
       }

        #clearing errros and warnings before possible display
        clear -object_id $object_id
        set existing_ids [list]
        if {$size_of_errors > 0 || $size_of_warnings > 0} {
            set result "{\"success\": false, \"object_id\": \"$object_id\", \"action_type\": \"$action_type\", \"modal_title\": \"$modal_title\", \n\"message\": \"\",\n\"warnings\": \[\n$json_warnings\n\],\n\"errors\": \[\n$json_errors\n\]\n}"
            im_rest_doc_return 200 "application/json" $result
            return
            ad_script_abort
        }
        
    }

    ad_proc display_all_existing_errors_and_warnings {
        -object_ids
        {-action_type ""}
        {-modal_title ""}
    } {
        displaying errors and warnings for given @param object_ids (list) in case any problems appeared
    } {

        variable objects_warnings
        variable objects_errors
        
        set obj_ctr_errors 0
        set obj_ctr_warnings 0
        set json_errors ""
        set json_warnings ""

        set size_of_warnings 0
        set size_of_errors 0



        foreach object_id $object_ids {

            # building json structure for warnings
            set size_of_warnings [dict size $objects_warnings]
            if {$size_of_warnings > 0} {
                set single_object_warnings [dict get $objects_warnings $object_id]
                dict for {field warning} $single_object_warnings {
                    set komma ",\n"
                    if {0 == $obj_ctr_warnings} { set komma "" }
                    if {$field ne ""} {
                        append json_warnings "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $warning]\"}"
                    } else {
                        append json_warnings "$komma{\"message\": \"[im_quotejson $warning]\"}"
                    }
                    incr obj_ctr_warnings
                }
            }
           # building json structure for errors
           set size_of_errors [dict size $objects_errors]
           if {$size_of_errors > 0} {
               set single_object_errors [dict get $objects_errors $object_id]
               dict for {field error} $single_object_errors {
                   set komma ",\n"
                   if {0 == $obj_ctr_errors} { set komma "" }
                   if {$field ne ""} {
                       append json_errors "$komma{\"element\": \"$field\", \"message\": \"[im_quotejson $error]\"}"
                    } else {
                       append json_errors "$komma{\"message\": \"[im_quotejson $error]\"}"
                    }
                   incr obj_ctr_errors
               }
           }

        }

        #clearing errros and warnings before possible display
        if {$size_of_errors > 0 || $size_of_warnings > 0} {

            set object_ids [list]
            set result "{\"success\": false, \"object_id\": \"$object_id\", \"action_type\": \"$action_type\", \"modal_title\": \"$modal_title\", \n\"message\": \"\",\n\"warnings\": \[\n$json_warnings\n\],\n\"errors\": \[\n$json_errors\n\]\n}"
            im_rest_doc_return 200 "application/json" $result

            # Clear
            foreach object_id $object_ids {
                clear -object_id $object_id
            }

            return
            ad_script_abort
        }
        
    }

}


# ---------------------------------------------------------------
# Projects endpoint
# ---------------------------------------------------------------

ad_proc -public im_rest_post_object_type_sencha_project {
    -company_id
    -project_type_id
    -source_language_id:required
    -target_language_ids
    { -subject_area_id ""}
    { -final_company_id ""}
    { -customer_contact_id ""}
    { -uploadedFiles ""}
    { -projectTasks ""}
    { -language_experience_level_id ""}
    { -project_id ""}
    { -expected_quality_id ""}
    { -project_name ""}
    { -project_nr ""}
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -debug 0 }
    { -query_hash_pairs ""}
    { -do_analysis_p 0 }
} {
    Handler for POST calls on the project. Will use any variable passed through the query_hash_pairs to create / update the project
    if the variable has been defined as attributes for the object_type.

    For variables starting with "skill_", these will be added as freelancer skills to the project. So are source_language_id, target_language_ids, subject_area_id and
    language_experience_level_id (the level for the skills in languages).

    @param company_id Company in which to create the project
    @param project_type_id Project Type of the project to create
    @param source_language_id Source Language for the project. Typically the language the source material is in
    @param target_language_ids Target Languages into which to translate the source material
    @param subject_area_id Subject Area of the project (determines pricing among other things)
    @param final_company_id Final company for this translation (determines the reference material and Termbase / Translation Memory to use)
    @param customer_contact_id Contact in the company who requested this project
    @param uploadedFiles Path of files which were uploaded previously for this project
    @param projectTasks Array of tasks
    @param language_experience_level_id Required experience level for this project. Used (previously) to differentiate certified translations
    @param project_id ID of the project. Useful if you edit instead of create a new project
    @param project_name Name of the project
    @param project_nr Number of the project. Mostly automatically determined now though
    @param query_harsh_pairs Pairs of variables passed onto the submit form not defined as explicit switches
    @oaram do_analysis_p Switch which decided if we should execute MemoQ file analysis. 1 = Yes, 0 = No.
} {
    ns_log Notice "im_rest_post_sencha_project: rest_oid=$rest_oid with query_hash $query_hash_pairs"
   
    # Extract a key-value list of variables from JSON POST request
    set random_errors_token "[ad_generate_random_string]"
  
    #-------------------------------------------------------
    # Check if we are creating new project or editing existing
    set project_exists_p 0
    if {$project_id ne ""} {
        if {[ad_var_type_check_number_p $project_id]} {
            set project_exists_p [db_string project_exists "
                        select count(*)
                        from im_projects
                        where project_id = :project_id
                "]
        } else {
            set project_name $project_id
        }
    }
    
    if {$rest_user_id eq 0} {
        set rest_user_id [auth::get_user_id]
    }
    
    if {$project_exists_p eq 1} {
    
        set sql "update im_projects set"
    
        if {$final_company_id ne ""} {
            append sql " final_company_id=:final_company_id,\n"
        }
        if {$company_id ne ""} {
            append sql " company_id=:company_id,\n"
        }
        if {$project_type_id ne ""} {
            append sql " project_type_id=:project_type_id,\n"
        }
        if {$project_nr ne ""} {
            append sql " project_nr=:project_nr,\n"
        }
        if {$project_name ne} {
            append sql " project_name=:project_name,\n"
        }
        if {$customer_contact_id ne ""} {
            append sql " company_contact_id=:customer_contact_id,\n"
        }
        if {$expected_quality_id ne ""} {
            append sql "expected_quality_id=:expected_quality_id,\n"
        }
        if {$subject_area_id ne ""} {
            append sql " subject_area_id=:subject_area_id,\n"
        }
        if {$source_language_id ""} {
            append sql " source_language_id=:source_language_id,\n"
        }
    
        append sql " project_id=:project_id where project_id=:project_id"
    
        set target_language_ids [split $target_language_ids ","]
        db_transaction {
            db_dml update_im_projects $sql
            db_dml delete_im_target_language "delete from im_target_languages where project_id=:project_id"
        }
    
        db_transaction {
            foreach target_language_id $target_language_ids {
                db_dml insert_im_target_language "insert into im_target_languages values ($project_id, $target_language_id)"
            }
        }
    
        if {[apm_package_installed_p "intranet-freelance"]} {
        
            db_dml delete_exising_skills "delete from im_object_freelance_skill_map where object_id =:project_id"
            
            #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
            if {$language_experience_level_id ne ""} {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
            }
                
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_source_language] -skill_ids $source_language_id
            im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_subject_area] -skill_ids $subject_area_id
            #im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_expected_quality] -skill_ids $expected_quality_id
            
            foreach target_language_id $target_language_ids {
                im_freelance_add_required_skills -object_id $project_id -skill_type_id [im_freelance_skill_type_target_language] -skill_ids $target_language_id
            }
        }
    } else {
        
#       sencha_errors::display_errors_and_warnings -object_id $random_errors_token
      
        set project_id [cog::trans::project::new \
            -company_id $company_id \
            -source_language_id $source_language_id \
            -target_language_ids $target_language_ids \
            -creation_user $rest_user_id \
            -project_name $project_name \
            -project_nr $project_nr \
            -project_type_id $project_type_id \
            -project_lead_id [ad_conn user_id] \
            -subject_area_id $subject_area_id \
            -final_company_id $final_company_id \
            -no_callback]
    
        if {$project_id eq 0} {
            set error_msg "Failed to create project. We sadly don't know why"
            sencha_errors::add_error -object_id $random_errors_token -problem $error_msg
            set redirect_url "/intranet"            
        } else {
            
            # Update project with values found in the hash array
            im_rest_object_type_update_sql \
                -rest_otype im_project \
                -rest_oid $project_id \
                -hash_array $query_hash_pairs
            
            # ---------------------------------------------------------------
            # Make sure we have the skills
            # ---------------------------------------------------------------
            if {[apm_package_installed_p "intranet-freelance"]} {
           
                # ---------------------------------------------------------------
                # We need to store source and target language again
                # ---------------------------------------------------------------
                
                #later we need to have possibiliy to return 2028 id by something like [im_freelance_skill_type_subject_area]
                if {$language_experience_level_id ne ""} {
                    im_freelance_add_required_skills -object_id $project_id -skill_type_id 2028 -skill_ids $language_experience_level_id
                }
                
                
                foreach key [array names hash_array] {
                    if {[string match "skill_*" $key]} {
                        set skill_type_id [lindex [split $key "_"] 1]
                        im_freelance_add_required_skills -object_id $project_id -skill_type_id $skill_type_id -skill_ids  $hash_array($key)
                    }
                }
            }
       
             # We need to catch the audit, as the project might still be created
            cog_log Notice "INVOKING after_create for $project_id"
            cog::callback::invoke -object_id $project_id -action after_create
            
            # ---------------------------------------------------------------
            # Deal with the uploaded files array
            # ---------------------------------------------------------------
            if {$uploadedFiles ne ""} {
                set uploaded_files_original $uploadedFiles
                set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
                set project_dir [cog::project::path -project_id $project_id]
                set source_dir "${project_dir}/$source_folder"
                foreach uploadedFiles [lindex $uploadedFiles 1] {
                    array set one_file [lindex $uploadedFiles 1]
                    file rename -force $one_file(path) "${source_dir}/$one_file(file)"
                }
                
                #callbacks to MemoQ & Trados
                if {$do_analysis_p == 1} {
                    catch {cog::callback::invoke -action after_file_upload -object_id $project_id -type_id $project_type_id -status_id [im_project_status_potential]}
                    ns_log Notice "Yes for MemoQ analysis"
                } else {
                    ns_log Notice "No for MemoQ analysis"
                }                

                foreach file [lindex $uploaded_files_original 1] {
                    array set one_file [lindex $file 1]
                    set task_filename $one_file(file)
                    set count_created_tasks [db_string count_created_tasks "select count(*) from im_trans_tasks where project_id = :project_id and task_filename =:task_filename " -default 0]
                    if {$count_created_tasks eq 0} {
                        cog::trans::task::new \
                            -project_id $project_id \
                            -task_name $task_filename \
                            -target_language_ids $target_language_ids
                    }
                }
            }

            # ---------------------------------------------------------------
            # Create tasks if necessary
            # ---------------------------------------------------------------
            if {$projectTasks ne ""} {
                foreach projectTask [lindex $projectTasks 1] {
                    array unset one_task
                    array set one_task [lindex $projectTask 1]
                    
                    if {[info exists one_task(units_of_measure)]} {
                        set task_uom_id $one_task(units_of_measure)
                    } else {
                        set task_uom_id 324 ;# Source words
                    }
                    
                    if {[info exists one_task(name)]} {
                        # Insert into the project
                        if {[info exists one_task(description)]} {
                            set description $one_task(description)
                        } else {
                            set description ""
                        }
                        
                        if {[info exists one_task(units)]} {
                            set billable_units $one_task(units)
                        } else {
                            set billable_units ""
                        }
                        set task_name $one_task(name)
                        cog::trans::task::new \
                            -project_id $project_id \
                            -task_name $task_name \
                            -task_uom_id $task_uom_id \
                            -task_type_id $project_type_id \
                            -description $description \
                            -target_language_ids $target_language_ids \
                            -billable_units $billable_units
                    
                    }
                }
            }

            set redirect_url [export_vars -base "/intranet/projects/view" -url {project_id}]
            set create_quote_p [db_string tasks_exist "select 1 from im_trans_tasks where project_id = :project_id limit 1" -default 0]            
            if {$create_quote_p} {
                set quote_id [cog::trans::invoice::create_from_tasks -project_id $project_id -cost_type_id [im_cost_type_quote]]
                if {$quote_id ne ""} {
                    set redirect_url [im_biz_object_url $quote_id]
                }
            }
            
            # Calculate the end date
            im_translation_update_project_dates -project_id $project_id
        }
    }

    set hash_array(rest_oid) "$project_id"
    #set hash_arry(project_id) "$project_id"

    if {![info exists redirect_url]} {
        set redirect_url [export_vars -base "/intranet/projects/view" -url {project_id}]
    }
    set hash_array(redirect_url) $redirect_url
    return [array get hash_array]
}

# ---------------------------------------------------------------
# Company Adding endpoint
# ---------------------------------------------------------------

ad_proc -public im_rest_post_object_type_sencha_company {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on companies.
    Can either create new company (with user) and edit existing one

    @param company_id Id of company in case we are editing existing one
    @param first_names First name of user in case we create new company
    @param last_name Last name of user in case we create new company
    @param email Email of user in case we create new company
    @param company_type_id Type of company
    @param country Country of company main_office_id
    @param streeet Street (address_line_1) of company main office
    @param city of company main office
    @zipcode zip code / postal code of company main office
    @website company website
    @vat_number company vat number
    @iban company iban
    @bic company bic
    @paypal company paypal email
    @skrill company skrill email
    @salutation company salutation used in email templates
    @mobile_phone mobile phone of user
    @fax company fax
    @skype  company skype
    @proz_url company proz_url
} {
    ns_log Notice "im_rest_post_sencha_company: rest_oid=$rest_oid"

    # Permissions
    # ToDo

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]


    # Check that all required variables are there
    set random_errors_token "[ad_generate_random_string]"
    set required_vars [list first_names last_name email]
    foreach var $required_vars {
        if {![info exists hash_array($var)] || $hash_array($var) eq ""} {
            sencha_errors::add_error -object_id $random_errors_token -field $var -problem "is required"
        } else {
            set $var $hash_array($var)
        }
    }
    
    set optional_vars [list company_id company_name company_type_id country street city country zipcode website vat_number iban bic paypal skrill salutation phone mobile_phone fax skype proz_url]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }

    sencha_errors::display_errors_and_warnings -object_id $random_errors_token


    # Check if we are creating a new company or editing an existing one:
    set company_exists_p 0
    if {$company_id ne "" && $company_name eq ""} {
        if {[ad_var_type_check_number_p $company_id]} {
            set company_name [db_string company_exists "
                    select company_name
                    from im_companies
                    where company_id = :company_id
            " -default ""]
        } else {
            # The APP provides the company_name as company_id
            if {$company_name eq ""} {
                set company_name $company_id
                set company_id ""
            }
        }
    }

    set company_id [db_string company_id "select company_id from im_companies where company_name = :company_name" -default ""]
    set company_exists_p [expr {$company_id ne ""}]
    cog_log Notice "COMPANY NAME FROM SENCHA ... $company_id .... $company_name ... $company_exists_p"
    #---------------------------------------------------------------
    # Permissions
    #---------------------------------------------------------------
    if {$company_exists_p} {
        if {[im_user_main_company_id -user_id $rest_user_id] ne $company_id} {
            # Check company permissions for this user
            im_company_permissions $rest_user_id $company_id view read write admin      
        } else {
            set write 1
        }

        if {!$write} {
            sencha_errors::add_error -object_id $random_errors_token -problem "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
            sencha_errors::display_errors_and_warnings -object_id $random_errors_token -action_type "show_modal_and_reload" -modal_title "Insufficient Privileges"
            return
        }
    } else {
        if {![im_permission $rest_user_id add_companies]} {
            sencha_errors::add_error -object_id $random_errors_token -problem "[_ intranet-core.lt_Insufficient_Privileg] [_ intranet-core.lt_You_dont_have_suffici]"
            sencha_errors::display_errors_and_warnings -object_id $random_errors_token -action_type "show_modal_and_reload" -modal_title "Insufficient Privileges"
            return
        }
    }


    if {$company_exists_p} {

        cog::company::update \
            -company_id         $company_id \
            -company_name       $company_name \
            -company_type_id    $company_type_id \
            -vat_number         $vat_number \
            -url                $website \
            -modifying_user $rest_user_id


    } else {
        set company_id [cog::company::new \
            -company_name       $company_name \
            -company_type_id    $company_type_id \
            -vat_number         $vat_number \
            -url                $website \
            -creation_user $rest_user_id]
    }

    # Update the office - Main office is auto generated by company::new
    set main_office_id [db_string main_office "select main_office_id from im_companies where company_id = :company_id" -default ""]
    cog::office::update \
        -office_id $main_office_id \
        -modifying_user $rest_user_id \
        -address_country_code $country \
        -address_line1 $street \
        -address_city $city \
        -address_postal_code $zipcode \
        -phone $phone


    # -----------------------------------------------------------------
    # Update the Company
    # -----------------------------------------------------------------
    set company_update_sql [list]
    foreach company_column [list paypal_email skrill_email vat_number iban bic] {    
        if {[im_column_exists im_companies $company_column]} {
            switch $company_column {
                paypal_email {
                    lappend company_update_sql "paypal_email = :paypal"
                }
                skrill_email {
                    lappend company_update_sql "skrill_email = :skrill"
                }
                default {
                    lappend company_update_sql "$company_column = :$company_column"
                }
            }
        }
    }
    
    # Run the update script
    if {[llength $company_update_sql]} {
        db_dml update_company "update im_companies set [join $company_update_sql ", "] where company_id = :company_id"
    }

    if {$company_exists_p} {
        cog::callback::invoke -object_id $main_office_id -action after_update
        cog::callback::invoke -object_id $company_id -action after_update
    } else {
        cog::callback::invoke -object_id $main_office_id -action after_create
        cog::callback::invoke -object_id $company_id -action after_create
    }

    # -----------------------------------------------------------------
    # Create user
    # -----------------------------------------------------------------
       
    set user_id [db_string first_last_name_exists_p "
        select  user_id
        from    cc_users
        where   lower(trim(first_names)) = lower(trim(:first_names)) and
            lower(trim(last_name)) = lower(trim(:last_name)) and
            lower(trim(email)) = lower(trim(:email))
        " -default ""]

       
    if {"" == $user_id} {
        # New user: create from scratch
        set email [string trim $email]
        set similar_user [db_string similar_user "select party_id from parties where lower(email) = lower(:email)" -default 0]
    
        if {$similar_user > 0} {
            set view_similar_user_link "<A href=/intranet/users/view?user_id=$similar_user>[_ intranet-core.user]</A>"
            sencha_errors::add_error -object_id $company_id -field "email" -problem "[_ intranet-core.Duplicate_UserB][_ intranet-core.lt_There_is_already_a_vi]"
            sencha_errors::display_errors_and_warnings -object_id $company_id
        }

        set company_contact_id [cog::company_contact::new -company_id $company_id \
            -first_names $first_names -last_name $last_name -email $email \
            -password "[ad_generate_random_string]" -creation_user_id $rest_user_id]

        if {$company_contact_id eq ""} {
            sencha_errors::add_error -object_id $company_id -field "email" -problem "Can't create user"
            sencha_errors::display_errors_and_warnings -object_id $company_id
        }
    
        cog::callback::invoke -object_id $company_contact_id -action after_create
    } else {

        set company_contact_id [cog::company_contact::update -company_id $company_id -contact_id $user_id \
            -first_names $first_names -last_name $last_name -email $email \
            -cell_phone $mobile_phone -work_phone $fax -home_phone $phone]
        cog::callback::invoke -object_id $company_contact_id -action after_update
    }

    if {[im_column_exists persons proz_url]} {
        db_dml update_persons "update persons set proz_url = :proz_url where person_id = :user_id"
    }    
    
    set hash_array(rest_oid) "$company_id"
    set hash_array(object_id) "$company_id"
    set hash_array(created_user_id) "$company_contact_id"
    set hash_array(company_name) "$company_name"
    set hash_array(company_id) "$company_id"
    set hash_array(main_office_id) "$main_office_id"
    return [array get hash_array]
}


ad_proc -public im_rest_post_object_type_sencha_email {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list subject body to_addr]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }
    
    set optional_vars [list from_addr upload_file company_id]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }

    if {$from_addr eq ""} {
        set from_addr [cc_email_from_party $rest_user_id]
    }

    # Insert the uploaded file linked under the package_id
    set package_id [apm_package_id_from_key "intranet-sencha-tables"]
    
    intranet_chilkat::send_mail \
        -to_addr $to_addr \
        -from_addr "$from_addr" \
        -subject "$subject" \
        -body "$body" \
        -object_id $company_id 


    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_object_type_sencha_contact_again {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id contact_again_date]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }
    
    db_dml update_company "update im_companies set contact_again_date = to_date(:contact_again_date,'DD.MM.YYYY') where company_id = :company_id"

    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_object_type_sencha_company_status {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id company_status_id]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }

    db_dml update_company "update im_companies set company_status_id = :company_status_id where company_id = :company_id"

    set hash_array(rest_oid) "$company_id"
    set hash_array(company_id) "$company_id"

    return [array get hash_array]
}


ad_proc -public im_rest_post_object_type_sencha_note {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on the project
} {
    ns_log Notice "im_rest_post_sencha_email: rest_oid=$rest_oid"

    # Extract a key-value list of variables from JSON POST request
    array set hash_array [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]

    # Check that all required variables are there
    set required_vars [list company_id note]
    foreach var $required_vars {
        if {![info exists hash_array($var)]} {
            return [im_rest_error -format $format -http_status 406 -message "Variable '$var' not specified. The following variables are required: $required_vars"]
        } else {
            set $var $hash_array($var)
        }
    }

    set optional_vars [list note_id note_type_id]
    foreach var $optional_vars {
        if {![info exists hash_array($var)]} {
            set $var ""
        } else {
            set $var $hash_array($var)
        }
    }
    
    set note [string trim $note]
    
    if {$note_id eq ""} {
        set note [template::util::richtext::create $note "text/html" ]
        set duplicate_note_sql "select  count(*)
            from    im_notes
            where   object_id = :company_id and note = :note
        "
    
        if {[db_string dup $duplicate_note_sql]} {
            im_rest_error -format json -http_status 403 -message "The note already exists for company_id $company_id"
        }
    
        if {$note_type_id eq ""} {set note_type_id [im_note_type_other]}
    
        set note_id [db_exec_plsql create_note "
            SELECT im_note__new(
                NULL,
                'im_note',
                now(),
                :rest_user_id,
                '[ad_conn peeraddr]',
                null,
                :note,
                :company_id,
                :note_type_id,
                [im_note_status_active]
            )
        "]
    } else {
        ns_log Debug "Need updateing the node $note_id"
    }

    set hash_array(rest_oid) "$note_id"
    set hash_array(note_id) "$note_id"

    return [array get hash_array]
}

ad_proc -public im_rest_post_error {
    -error_list
    {-message "Error in Fields"}
} {
    Return the JSON with the errors for each field. The error_list is usually appended when we do the checking
    
    @error_list List of "field" & "error messages" for each field that has been throwing an error
} {

    set obj_ctr 0
    set errors ""
    foreach error $error_list {
        set komma ",\n"
        if {0 == $obj_ctr} { set komma "" }
        append errors "$komma{\"element\": \"[im_quotejson [lindex $error 0]]\", \"error\": \"[im_quotejson [lindex $error 1]]\"}"
        incr obj_ctr
        
    }
    set result "{\"success\": false,\n\"message\": \"[im_quotejson $message]\",\n\"errors\": \[\n$errors\n\]\n}"
}

ad_proc -public im_rest_post_object_type_sencha_freelancer {
    { -format "json" }
    { -rest_user_id 0 }
    { -rest_otype "" }
    { -rest_oid "" }
    { -content "" }
    { -debug 0 }
    { -query_hash_pairs ""}
} {
    Handler for POST calls on freelancer DB
} {

    set success 1
    set error ""

    array set query_hash [im_rest_parse_json_content -rest_otype $rest_otype -format $format -content $content]
    
    set random_errors_token "[ad_generate_random_string]"

    # Setting 'required' variables
    #set required_vars [list first_names last_name email email company_type_id source_language_ids target_languages_ids country]
    set required_vars [list first_names last_name email company_type_id country source_language_ids target_language_ids]
    foreach var $required_vars {
        if {![info exists query_hash($var)] || $query_hash($var) eq ""} {
            sencha_errors::add_error -object_id $random_errors_token -field "$var"  -problem "this is required field"
        } else {
            set $var $query_hash($var)
        }
    }

    # Setting 'optional' variables
    set optional_vars [list freelancer_id company_name telephone address zipcode city cert_source_language_ids cert_target_language_ids subject_area_ids note submitted_prices]
    foreach var $optional_vars {
        if {![info exists query_hash($var)]} {
            set $var ""
        } else {
            set $var $query_hash($var)
        }
    }

    set include_prices_p 0

   

    if {$submitted_prices ne ""} {
        set include_prices_p 1
    }

    sencha_errors::display_errors_and_warnings -object_id $random_errors_token

    if {$company_name eq ""} {
        set company_name "$first_names $last_name"
    }

    # skill type ids for both target and source language
    set source_language_skill_type_id [im_freelance_skill_type_source_language]
    set target_language_skill_type_id [im_freelance_skill_type_target_language]
    set subject_area_skill_type_id [im_freelance_skill_type_subject_area]
            
    # we need to buil lists, because frontend sends languages as string (comma seperated language ids)
    set source_languages_list_ids [list]
    set target_languages_list_ids [list]
    set cert_source_language_list_ids [list]
    set cert_target_language_list_ids [list]
    set subject_area_list_ids [list]

    foreach source_language_id [split $source_language_ids ","] {
        lappend source_languages_list_ids $source_language_id
    }

    foreach target_language_id [split $target_language_ids ","] {
        lappend target_languages_list_ids $target_language_id
    }

    foreach cert_source_language_id [split $cert_source_language_ids ","] {
        lappend cert_source_language_list_ids $cert_source_language_id
    }

    foreach cert_target_language_id [split $cert_target_language_ids ","] {
        lappend cert_target_language_list_ids $cert_target_language_id
    }

    foreach subject_area_id [split $subject_area_ids ","] {
        lappend subject_area_list_ids $subject_area_id
    }

    set unconfirmed_experience 2200
    set certified_experience 2204
    set added_new_freelancer_p 0

    if {$freelancer_id eq 0} {

        set freelancer_exist_p 0
        set company_exists_p 0

        set freelancer_id_check [db_string first_last_name_exists_p "
        select  user_id as freelancer_id_check
        from    cc_users
        where   lower(trim(email)) = lower(trim(:email))
        " -default 0]

        if {$freelancer_id_check == 0} {
            set $freelancer_exist_p 0
        } else {
            set error "Such email already exists"
            set email_exists_redirect_url "[ad_url][apm_package_url_from_key "intranet-core"]users/view?user_id=$freelancer_id_check"
            set result "{\"success\":false , \"error\": \"$error\", \"redirect_url\":\"[im_quotejson $email_exists_redirect_url]\", \"error_type\":\"user_exists\", \"existing_user_email\":\"$email\"}"
            im_rest_doc_return 200 "application/json" $result
            return
        }

        # taking care of users table

        if {$freelancer_exist_p == 0} {

           # creating new user
            set username [string tolower $email]
            set screen_name "$first_names $last_name"
            set password [ad_generate_random_string]
            set password_confirm $password

            array set creation_info [auth::create_user \
                     -verify_password_confirm \
                     -username $username \
                     -email $email \
                     -first_names $first_names \
                     -last_name $last_name \
                     -screen_name $screen_name \
                     -password $password \
                     -password_confirm $password_confirm \
                     ]
            set creation_status $creation_info(creation_status)
        

            if {[info exists creation_info(user_id)]} {
                set added_new_freelancer_p 1
                set user_id $creation_info(user_id)
                set freelancer_id $user_id
                set profile_id [im_profile_freelancers]
                im_profile::add_member -profile_id $profile_id -user_id $user_id
            } else {
                set user_id 0
            }

            if {$user_id ne 0} {
                set registered_users [db_string registered_users "select object_id from acs_magic_objects where name='registered_users'"]
                set reg_users_rel_exists_p [db_string member_of_reg_users "
                    select  count(*)
                    from    group_member_map m, membership_rels mr
                    where   m.member_id = :user_id
                        and m.group_id = :registered_users
                        and m.rel_id = mr.rel_id
                        and m.container_id = m.group_id
                        and m.rel_type::text = 'membership_rel'::text
               "]
                if {!$reg_users_rel_exists_p} {
                    relation_add -member_state "approved" "membership_rel" $registered_users $user_id
                }

                # we create two helping lists (for each langauge type), so that we make sure same language isn't added twice
                set already_added_source_languages [list]
                set already_added_target_languages [list]

                # adding certified source languages
                foreach cert_source_language_id $cert_source_language_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :cert_source_language_id, :source_language_skill_type_id, :certified_experience)"
                    if {[lsearch $already_added_source_languages $cert_source_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_source_languages $cert_source_language_id
                }

                # adding certified target languages
                foreach cert_target_language_id $cert_target_language_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :cert_target_language_id, :target_language_skill_type_id, :certified_experience)"
                    if {[lsearch $already_added_target_languages $cert_target_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_target_languages $cert_target_language_id
                }

                # adding source languages
                foreach source_language_id $source_languages_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :source_language_id, :source_language_skill_type_id, :unconfirmed_experience)"
                    if {[lsearch $already_added_source_languages $source_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_source_languages $source_language_id
                }

                # adding target languages
                foreach target_language_id $target_languages_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :target_language_id, :target_language_skill_type_id, :unconfirmed_experience)"
                    if {[lsearch $already_added_target_languages $target_language_id] == -1} {
                        db_dml insert_freelance_skills $sql
                    }
                    lappend already_added_target_languages $target_language_id
                }

                # adding subject area skill ids
                foreach subject_area_id $subject_area_list_ids {
                    set sql "
                        insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                        values (:user_id, :subject_area_id, :subject_area_skill_type_id, :unconfirmed_experience)"
                    db_dml insert_freelance_skills $sql
                }

                # Taking care of Company of freelancer
                regsub -all {[^a-zA-Z0-9]} [string trim [string tolower $company_name]] "_" company_path
    
                if {$company_exists_p eq 0} {
                    # Check if we have the path
                    set company_id [db_string company_exists "select company_id from im_companies where company_path = :company_path" -default 0]
                    if {$company_id ne 0} {
                        set company_exists_p 1
                   }
                }

                if {$company_exists_p eq 0} {
                    # default company status
                    set company_status_id [im_company_status_active]
                    set company_type_id [im_company_type_provider]

                    db_transaction {

                        set company_id [im_company::new \
                            -company_name       $company_name \
                            -company_type_id    $company_type_id \
                            -company_status_id  $company_status_id \
                            -no_callback ]

                        # nowt we create ne office
                        set office_path "${company_path}_main_office"
                        set office_name "$company_name [lang::message::lookup "" intranet-core.Main_Office {Main Office}]"
                        set office_type_id [im_office_type_main]        

                        set main_office_id [im_office::new \
                            -office_name    $office_name \
                            -company_id     $company_id \
                            -office_type_id [im_office_type_main] \
                            -office_status_id   [im_office_status_active] \
                            -office_path    $office_path]


                        # udpate office addresses
                        db_dml update_office_addresses "update im_offices set contact_person_id= :user_id, phone =:telephone, address_line1 =:address, address_postal_code =:zipcode, address_country_code =:country, address_city =:city where office_id =:main_office_id"

                        # add users to the office as
                        set role_id [im_biz_object_role_office_admin]
                        im_biz_object_add_role $user_id $main_office_id $role_id

                        # saving rest_user_id as key account
                        set key_account_role_id [im_company_role_key_account]
                        im_biz_object_add_role $rest_user_id $company_id $key_account_role_id
                    
                    }
                    set company_exists_p 1
                } 

                # setting new user as accounting contact & primary_contact + adding him as company member
                db_dml update_primary_contact "update im_companies set primary_contact_id = :user_id where company_id = :company_id"
                set role_id [im_biz_object_role_full_member]
                im_biz_object_add_role $user_id $company_id $role_id
    
            }

        }
        # Taking care of Note saving
        set save_note_p 1
        set default_note_type_id [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "DefaultNoteTypeId" -default ""]
        if {$default_note_type_id eq ""} {
            # we retrieve "other" note type id
            set default_note_type_id [db_string default_note_type_id "select category_id as default_note_type_id from im_categories where category_type ='Intranet Notes Type' and category = 'Other'" -default ""]
            # if it still equals empty string, then we do not save note at all
            if {$default_note_type_id eq ""} {
                set save_note_p 0
            }
        }
        if {$save_note_p eq 1} {
            set note [template::util::richtext::create $note "text/html"]
            set note_id [db_exec_plsql create_note "
                SELECT im_note__new(
                    NULL,
                    'im_note',
                    now(),
                    :rest_user_id,
                    '[ad_conn peeraddr]',
                    null,
                    :note,
                    :user_id,
                    :default_note_type_id,
                    [im_note_status_active]
                    )
            "]
        }
        # taking care of prices
        if {$include_prices_p == 1 && $company_exists_p == 1} {
            set arr [lindex $submitted_prices 1]
            set currency "EUR"
            set valid_from ""
            set valid_through ""
            set subject_area_id ""
            set file_type_id ""
            set note ""
            for {set i 0} {$i < [llength $arr]} {incr i} {
                array set price_arr [lindex [lindex $arr $i] 1]
                set price_id [db_nextval "im_trans_prices_seq"]
                set uom_id $price_arr(uom_id)
                set task_type_id $price_arr(task_type_id)
                set price_source_language_id $price_arr(source_language_id)
                set price_target_language_id $price_arr(target_language_id)
                set price $price_arr(price)
                set min_price $price_arr(min_price)


                set sql "
                insert into im_trans_prices (price_id, uom_id, company_id, task_type_id, source_language_id, target_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note) 
                values (:price_id, :uom_id, :company_id, :task_type_id, :price_source_language_id, :price_target_language_id, :subject_area_id, :file_type_id, :valid_from, :valid_through, :currency, :price, :min_price, :note)"
                db_dml insert_freelance_skills $sql
            }
        }
    }
        
    if {$freelancer_id > 0 && $added_new_freelancer_p eq 0} {
        
        # updating user personal info
        # we need username
        db_1row freelancer_username "select username from cc_users where object_id=:freelancer_id"
        im_user_update_existing_user -user_id $freelancer_id -username $username -email $email -first_names $first_names -last_name $last_name

        # first we udpate company main office
        set freelancer_company_id [im_translation_freelance_company -freelance_id $freelancer_id]
        set company_data [db_0or1row freelancer_data "select company_name as old_company_name, main_office_id from im_companies where company_id =:freelancer_company_id"]
        set company_office_id $main_office_id
        db_dml update_company_main_office "update im_offices set address_line1 =:address, address_country_code=:country, address_postal_code=:zipcode, address_city=:city, phone=:telephone where office_id=:company_office_id"

        # updating user company info
        db_dml update_freelancer_company "update im_companies set company_name=:company_name where company_id=:freelancer_company_id"

        # updating user target and source languages
        # we first need to remove existing soutce and target languages + subject area skills
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:source_language_skill_type_id"
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:target_language_skill_type_id"
        db_dml delete_target_and_source_languages "delete from im_freelance_skills where user_id =:freelancer_id and skill_type_id =:subject_area_skill_type_id"

        # we create two helping lists (for each langauge type), so that we make sure same language isn't added twice
        set already_added_source_languages [list]
        set already_added_target_languages [list]

        # adding certified source languages
        foreach cert_source_language_id $cert_source_language_list_ids {
            set sql "
                insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                values (:freelancer_id, :cert_source_language_id, :source_language_skill_type_id, :certified_experience)"
            if {[lsearch $already_added_source_languages $cert_source_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_source_languages $cert_source_language_id
        }

        # adding certified target languages
            foreach cert_target_language_id $cert_target_language_list_ids {
                set sql "
                    insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
                    values (:freelancer_id, :cert_target_language_id, :target_language_skill_type_id, :certified_experience)"
            if {[lsearch $already_added_target_languages $cert_target_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_target_languages $cert_target_language_id
        }

        # adding source languages
        foreach source_language_id $source_languages_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :source_language_id, :source_language_skill_type_id, :unconfirmed_experience)"
            if {[lsearch $already_added_source_languages $source_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_source_languages $source_language_id
        }

        # adding target languages
        foreach target_language_id $target_languages_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :target_language_id, :target_language_skill_type_id, :unconfirmed_experience)"
            if {[lsearch $already_added_target_languages $target_language_id] == -1} {
                db_dml insert_freelance_skills $sql
            }
            lappend already_added_target_languages $target_language_id
        }
        
        # adding subject area skills
        foreach subject_area_id $subject_area_list_ids {
            set sql "
            insert into im_freelance_skills (user_id, skill_id, skill_type_id, confirmed_experience_id) 
            values (:freelancer_id, :subject_area_id, :subject_area_skill_type_id,:unconfirmed_experience)"
            db_dml insert_freelance_skills $sql
        }

        # Taking care of notes when saving
        # First we check if submitted note has any content
        if {[string length $note] > 0} {
            set save_note_p 1
        } else {
            set save_note_p 0
        } 

         # Then if note has some content, we need to make sure that we actually have 'DefaultNoteType' set up in system
         # Possible change: so far it always proceeds, because if no 'DefaultNoteType' is found, we use 'Other' as type
        if {$save_note_p eq 1} {
            set default_note_type_id [parameter::get_from_package_key -package_key "intranet-sencha-tables" -parameter "DefaultNoteTypeId" -default ""]
            if {$default_note_type_id eq ""} {
                # we retrieve "other" note type id
                set default_note_type_id [db_string default_note_type_id "select category_id as default_note_type_id from im_categories where category_type ='Intranet Notes Type' and category = 'Other'" -default ""]
                # if it still equals empty string, then we do not save note at all
                if {$default_note_type_id eq ""} {
                    set save_note_p 0
                }
            }

        }

        # if NoteType exist, then we check if freelaner alraedy has such note
        if {$save_note_p eq 1} {
            set note [template::util::richtext::create $note "text/html"]
            set count_freelancer_notes [db_string count_freelancer_notes "select count(*) from im_notes where object_id = :freelancer_id and note_type_id=:default_note_type_id" -default 0]
            # if no notes of that type for that FL exist, we create a new one
            if {$count_freelancer_notes == 0} {
                set note_id [db_exec_plsql create_note "
                    SELECT im_note__new(
                        NULL,
                        'im_note',
                        now(),
                        :rest_user_id,
                        '[ad_conn peeraddr]',
                        null,
                        :note,
                        :freelancer_id,
                        :default_note_type_id,
                        [im_note_status_active]
                    )"
                ]
            } else {
                # such note already exist, getting its ID
                db_0or1row existing_note_id "select note_id as existing_note_id from im_notes where object_id = :freelancer_id and note_type_id=:default_note_type_id"
                if {$existing_note_id} {
                    db_dml update_exsting_note "update im_notes set note =:note where note_id =:existing_note_id"
                }
            }
        }

        # taking care of prices
        if {$include_prices_p} {
            db_dml delete_freelancer_prices "delete from im_trans_prices where company_id =:freelancer_company_id"
            set arr [lindex $submitted_prices 1]
            set currency "EUR"
            set valid_from ""
            set valid_through ""
            set subject_area_id ""
            set file_type_id ""
            set note ""
            for {set i 0} {$i < [llength $arr]} {incr i} {
                array set price_arr [lindex [lindex $arr $i] 1]
                set price_id [db_nextval "im_trans_prices_seq"]
                set uom_id $price_arr(uom_id)
                set task_type_id $price_arr(task_type_id)
                set price_source_language_id $price_arr(source_language_id)
                set price_target_language_id $price_arr(target_language_id)
                set price $price_arr(price)
                set min_price $price_arr(min_price)


                set sql "
                insert into im_trans_prices (price_id, uom_id, company_id, task_type_id, source_language_id, target_language_id, subject_area_id, file_type_id, valid_from, valid_through, currency, price, min_price, note) 
                values (:price_id, :uom_id, :freelancer_company_id, :task_type_id, :price_source_language_id, :price_target_language_id, :subject_area_id, :file_type_id, :valid_from, :valid_through, :currency, :price, :min_price, :note)"
                db_dml insert_freelance_skills $sql
            }
        }

        set user_id $freelancer_id

    }


    im_rest_get_custom_sencha_single_freelancer -query_hash_pairs [list freelancer_id $freelancer_id]

    #set result "{\"success\":true , \"error\": \"$error\"}"
    #im_rest_doc_return 200 "application/json" $result
    #return

    set hash_array(rest_oid) $freelancer_id
        
    return [array get hash_array]

}


