ad_proc -public -callback im_project_after_file_upload {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Callback that is executed after MemoQ or Trados file upload
} -

ad_proc -public -callback im_project_after_quote2order {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Callback that is executed after the project status changes from quote2order
} -

ad_proc -public -callback im_project_after_open2delivered {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Callback that is executed after the project status changes from 'Open' to 'Delivered'
} -

ad_proc -public -callback im_project_after_potential2canceled {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Callback that is executed after the project status changes from 'Potential' to 'Canceled'
} -