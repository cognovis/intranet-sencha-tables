ad_page_contract {
    @author ND
} {
}

set user_id [ad_maybe_redirect_for_registration]

# remove RFQ notifications, temporarily we use package_id here. This should be changed later
set package_id [apm_package_id_from_key "sencha-freelance-translation"]
db_dml update_notifications "update im_freelance_notifications set viewed_date = now() where object_id = :package_id and viewed_date is null"

if {[apm_package_installed_p "sencha-portal"]} {
    set portal_url [parameter::get_from_package_key -package_key "sencha-portal" -parameter ProjectManagersPortalUrl -default ""]
} else {
    set portal_url ""
}

if {$portal_url ne ""} {
    ad_returnredirect "${portal_url}/#rfq"
}


