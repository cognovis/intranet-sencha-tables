ad_page_contract {
    upload_file
} {
}

# Upload a file to the backend. This can be any file. Needs to be handled later in a different procedure.

catch {
    set tmp_filename [ns_queryget upload_file.tmpfile]
	set filename [ns_queryget upload_file]
	file copy $tmp_filename "${tmp_filename}.copy"

} errmsg

if {[string length $errmsg] > 0} {
	ns_log Warning "file-upload.tcl: problems uploading a file: [ns_queryget upload_file.tmpfile]"
	set result "{\"success\": false,\n\"total\": 0,\n\"message\":\"Could not upload file: [im_quotejson $errmsg]\"}"
} else {
	set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"File: Data loaded\",\n\"data\": \[\n{\"file\":\"$filename\",\"path\":\"${tmp_filename}.copy\"}\n\]\n}"
}

im_rest_doc_return 200 "application/json" $result
return
