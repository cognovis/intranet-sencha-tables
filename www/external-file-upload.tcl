ad_page_contract {
    external_upload_file
} {
}


set rand_token [ad_generate_random_string 3]

set base_path_unix [parameter::get -package_id [im_package_filestorage_id] -parameter "ProjectBasePathUnix" -default "/tmp/projects"]

set external_uploads_folder "$base_path_unix/external_rfq"
file mkdir $external_uploads_folder

set tmp_filename [ns_queryget upload_file.tmpfile]
set filename "$rand_token[ns_queryget upload_file]"

catch {

	file copy $tmp_filename "${tmp_filename}.copy"
	set new_file_path "$external_uploads_folder/$filename"
	file copy $tmp_filename $new_file_path

} errmsg

if {[string length $errmsg] > 0} {
	ns_log Warning "external-file-upload.tcl: problems uploading a file: $rand_token[ns_queryget upload_file]"
    set result "{\"success\": false,\n\"total\": 1,\n\"message\":\"Could not upload file: [im_quotejson $errmsg]\"}"
} else {
    set result "{\"success\": true,\n\"total\": 1,\n\"message\": \"File: Data loaded\",\n\"data\": \[\n{\"path\":\"$filename\", \"original_filename\":\"[ns_queryget upload_file]\", \"extension\":\"[file extension $filename]\", \"size\":\"[file size $new_file_path]\" }\n\]\n}"
}

im_rest_doc_return 200 "application/json" $result
return
