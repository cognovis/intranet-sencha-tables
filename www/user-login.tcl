ad_page_contract {
    Return the user_id of the currently logged in user

    @param username username to use for trying to login the user
    @param password password to use
} {
    { username ""}
    { password ""}
}

if {$username ne "" && $password ne ""} {
    # Try to login the user
    array set auth_info [auth::authenticate -username $username -password $password]
    ns_log Notice "Arr... [array get auth_info]"
    if {$auth_info(auth_status) ne "ok"} {
        im_rest_doc_return 200 "application/json" "{\"success\": false,\n\"message\": \"$auth_info(auth_message)\"\n}"
    } else {
        set token [im_generate_auto_login -user_id $auth_info(user_id)]
        im_rest_doc_return 200 "application/json" "{\"success\": true,\n\"token\": \"$token\",\n\"message\": \"user logged in\", \n\"user_id\": $auth_info(user_id)}"
    }
} else {
    set user_id [auth::get_user_id]

    if {$user_id ne 0} {
        im_rest_doc_return 200 "application/json" "{\"success\": true,\n\"message\": \"user logged in\", \n\"user_id\": $user_id}"
    } else {
        im_rest_doc_return 200 "application/json" "{\"success\": false,\n\"message\": \"user not logged in\"\n}"
    }
}
