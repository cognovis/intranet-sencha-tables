ad_page_contract {
    Shows all projects. Lots of dimensional sliders

    @param status_id if specified, limits view to those of this status
    @param type_id   if specified, limits view to those of this type
} {
    { project_status_id:integer "[im_project_status_open]" }
    { project_type_id:integer "" }
    { project_lead_id:integer ""}
    { customer_id:integer ""}
    { new_project_p:integer 0}
}

set user_id [ad_maybe_redirect_for_registration]

