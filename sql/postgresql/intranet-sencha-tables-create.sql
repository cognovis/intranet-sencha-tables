-- /packages/intranet-sencha-tables/sql/postgresql/intranet-sencha-tables-create.sql
--
-- Copyright (c) 2016 cognovis GmbH + ]project-open[
--
--
-- @author malte.sussdorff@cognovis.de

-----------------------------------------------------------
--

-- Update the menus for companies
update im_menus set url = '/sencha-tables/companies/index' where label = 'companies';
update im_menus set sort_order=2, url = '/sencha-tables/companies/index?company_status_id=41&company_type_id=57' where label = 'customers_potential';
update im_menus set sort_order=1, url = '/sencha-tables/companies/index?company_status_id=46&company_type_id=57' where label = 'customers_active';
update im_menus set sort_order=3, url = '/sencha-tables/companies/index?company_status_id=48&company_type_id=57' where label = 'customers_inactive';


-- Provide permissions on im_categories to at least the employees in intranet-rest

-- Update the components
