SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.1.4-4.2.0.1.5.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin

     ALTER TABLE external_rfq ADD COLUMN status INTEGER;

return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();