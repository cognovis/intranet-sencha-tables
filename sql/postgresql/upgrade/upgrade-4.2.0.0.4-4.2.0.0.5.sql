SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.0.4-4.2.0.0.5.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin
    UPDATE im_freelance_skills 
    SET claimed_experience_id = 
        (CASE WHEN claimed_experience_id <> 2204 THEN 2200 ELSE claimed_experience_id END),
        confirmed_experience_id = 
    (CASE WHEN confirmed_experience_id <> 2204 THEN 2200 ELSE confirmed_experience_id END)
    WHERE skill_type_id in ('2000', '2002');
return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin
    UPDATE im_categories SET enabled_p = 'f' WHERE category_id in ('2201', '2202', '2203');
return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();