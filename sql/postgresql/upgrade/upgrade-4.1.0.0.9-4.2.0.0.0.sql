SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.1.0.0.9-4.2.0.0.0.sql','');


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin

    create table external_rfq (
        rfq_id  serial
                    primary key,
        first_name varchar(80),
        last_name varchar(80),
        company_name varchar(80),
        email varchar(40),
        telephone varchar(30),
        quality_level varchar(20),
        project_type varchar(20),
        source_language varchar(20),
        target_languages varchar(200),
        matched_user_id integer,
        matched_company_id integer,
        comment varchar(300),
        creation_date timestamptz null
    );

    ALTER TABLE external_rfq ALTER COLUMN creation_date SET DEFAULT now();

    return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin

    create table external_rfq_files (
        rfq_file_id  serial
                    primary key,
        rfq_id          integer not null
                            constraint external_rfq_fl_rfq_id_fk
                            references external_rfq,
        filename varchar(80),
        original_filename varchar(80),
        extension varchar(10),
        size integer
    );



    return 1;

end;
$$ LANGUAGE 'plpgsql';


SELECT inline_0 ();
DROP FUNCTION inline_0 ();


CREATE OR REPLACE FUNCTION external_rfq__new(
   p_first_name varchar, 
   p_last_name varchar, 
   p_company_name varchar,
   p_email varchar,
   p_telephone varchar,
   p_quality_level varchar,
   p_project_type varchar,
   p_source_language varchar,
   p_target_languages varchar,
   p_matched_user_id integer,
   p_matched_company_id integer,
   p_comment varchar,
   p_creation_date timestamptz

) RETURNS integer AS $$
declare
new_rfq_id integer;
BEGIN
    insert into external_rfq (first_name, last_name, company_name, email, telephone, quality_level, project_type, source_language, target_languages, matched_user_id, matched_company_id, comment, creation_date) 
        values (p_first_name, p_last_name, p_company_name, p_email, p_telephone, p_quality_level, p_project_type, p_source_language, p_target_languages, p_matched_user_id, p_matched_company_id, p_comment, p_creation_date)
        returning external_rfq.rfq_id into new_rfq_id;
    return new_rfq_id;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION external_rfq_file__new(
   p_rfq_id integer, 
   p_filename varchar(80),
   p_original_filename varchar(80),
   p_extension varchar(80),
   p_size integer

) RETURNS integer AS $$
BEGIN
    insert into external_rfq_files (rfq_id, filename, original_filename, extension, size) 
    values (p_rfq_id, p_filename, p_original_filename, p_extension, p_size);
    return 1;
END;
$$ LANGUAGE plpgsql;


