SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.0.8-4.2.0.0.9.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin
UPDATE im_trans_prices
SET task_type_id = 
   (CASE 
   WHEN task_type_id = 4210 THEN 93
   WHEN task_type_id = 4218 THEN 2505 END);
return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();
