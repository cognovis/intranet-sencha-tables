SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.2.0.0.2-4.2.0.0.3.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
begin

     ALTER TABLE external_rfq ADD COLUMN cost_center_id integer;

return 1;

end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();


DROP FUNCTION IF EXISTS external_rfq__new(
   p_first_name varchar, 
   p_last_name varchar, 
   p_company_name varchar,
   p_email varchar,
   p_telephone varchar,
   p_quality_level varchar,
   p_project_type varchar,
   p_source_language varchar,
   p_target_languages varchar,
   p_matched_user_id integer,
   p_matched_company_id integer,
   p_comment varchar,
   p_creation_date timestamptz

);



CREATE OR REPLACE FUNCTION external_rfq__new(
   p_first_names varchar, 
   p_last_name varchar, 
   p_cost_center_id integer,
   p_company_name varchar,
   p_email varchar,
   p_telephone varchar,
   p_quality_level_id integer,
   p_project_type_id integer,
   p_source_language_id integer,
   p_target_language_ids varchar,
   p_matched_user_id integer,
   p_matched_company_id integer,
   p_comment varchar,
   p_creation_date timestamptz

) RETURNS integer AS $$
declare
new_rfq_id integer;
BEGIN
    insert into external_rfq (first_names, last_name, cost_center_id, company_name, email, telephone, quality_level_id, project_type_id, source_language_id, target_language_ids, matched_user_id, matched_company_id, comment, creation_date) 
        values (p_first_names, p_last_name, p_cost_center_id, p_company_name, p_email, p_telephone, p_quality_level_id, p_project_type_id, p_source_language_id, p_target_language_ids, p_matched_user_id, p_matched_company_id, p_comment, p_creation_date)
        returning external_rfq.rfq_id into new_rfq_id;
    return new_rfq_id;
END;
$$ LANGUAGE plpgsql;