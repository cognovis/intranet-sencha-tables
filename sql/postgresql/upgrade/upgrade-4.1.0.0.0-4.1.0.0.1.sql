--
--
--
-- Copyright (c) 2015, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author <yourname> (<your email>)
-- @creation-date 2013-01-19
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/intranet-sencha-tables/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

-- contact_again_date for companies


create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
        select count(*) into v_count from user_tab_columns
        where table_name = ''im_companies'' and column_name = ''contact_again_date'';
        if v_count > 0 then return 0; end if;

        alter table im_companies add column contact_again_date timestamptz;

        return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();



CREATE OR REPLACE FUNCTION inline_16 ()
RETURNS integer AS '
DECLARE
    v_acs_attribute_id	integer;
    v_attribute_id		integer;
    v_count			integer;
    row			record;
BEGIN


    SELECT attribute_id INTO v_acs_attribute_id FROM acs_attributes WHERE object_type = ''im_company'' AND attribute_name = ''contact_again_date'';

    IF v_acs_attribute_id IS NOT NULL THEN
       v_attribute_id := im_dynfield_attribute__new_only_dynfield (
           null,					-- attribute_id
           ''im_dynfield_attribute'',		-- object_type
           now(),					-- creation_date
           null,					-- creation_user
           null,					-- creation_ip
           null,					-- context_id
           v_acs_attribute_id,			-- acs_attribute_id
           ''jq_timestamp'',				-- widget
           ''f'',					-- deprecated_p
           ''t'',					-- already_existed_p
           null,					-- pos_y
           ''plain'',				-- label_style
           ''f'',					-- also_hard_coded_p
           ''t''					-- include_in_search_p
      );
    ELSE
      v_attribute_id := im_dynfield_attribute_new (
         ''im_company'',			-- object_type
         ''contact_again_date'',			-- column_name
         ''#intranet-sencha-tables.contact_again_date#'',	-- pretty_name
         ''jq_timestamp'',				-- widget_name
         ''date'',				-- acs_datatype
         ''f'',					-- required_p
         17,					-- pos y
         ''f'',					-- also_hard_coded
         ''im_companies''			-- table_name
      );

    END IF;


    FOR row IN
        SELECT category_id FROM im_categories WHERE category_type = ''Intranet Company Type''
    LOOP

        SELECT count(*) INTO v_count FROM im_dynfield_type_attribute_map WHERE attribute_id = v_attribute_id AND object_type_id = row.category_id;
        IF v_count = 0 THEN
           INSERT INTO im_dynfield_type_attribute_map
                 (attribute_id, object_type_id, display_mode, help_text,section_heading,default_value,required_p)
           VALUES
              (v_attribute_id, row.category_id,''edit'',null,null,null,''f'');
        ELSE
           UPDATE im_dynfield_type_attribute_map SET display_mode = ''edit'', required_p = ''f'' WHERE attribute_id = v_attribute_id AND object_type_id = row.category_id;
        END IF;

    END LOOP;

    RETURN 0;
END;' language 'plpgsql';

SELECT inline_16 ();
DROP FUNCTION inline_16 ();